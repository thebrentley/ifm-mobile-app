// Ionic Starter App

// angular.module is a global place for creating, registering and retrieving Angular modules
// 'starter' is the name of this angular module example (also set in a <body> attribute in index.html)
// the 2nd parameter is an array of 'requires'
// 'starter.controllers' is found in controllers.js
var months = ["January","February","March","April","May","June","July","August","September","October","November","December"];
angular.module('starter', ['ionic', 'starter.controllers', 'ion-floating-menu', 'ngCordova', 'ion-datetime-picker', 'uiGmapgoogle-maps'])

    .run(function ($ionicPlatform) {
        $ionicPlatform.ready(function () {
            // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
            // for form inputs)
            if (window.cordova && window.cordova.plugins.Keyboard) {
                cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
                cordova.plugins.Keyboard.disableScroll(true);

            }
            if (window.StatusBar) {
                // org.apache.cordova.statusbar required
                StatusBar.styleDefault();
            }
        });
    })

    .constant('config',
    {
        endpoint : 'http://farmersmarket.us-west-2.elasticbeanstalk.com'
    })

    .filter('monthDayString', function () {
        return function (date) {
            var d = new Date(date);
            var day = d.getDate();
            if (day == 1) day += "st";
            else if (day == 2) day += "nd";
            else if (day == 3) day += "rd";
            else day += "th";
            return months[d.getMonth()]+" "+ day;
        }
    })

    .filter('dayString', function () {
        return function (date) {
            var d = new Date(date);
            return d.getDate();
        }
    })

    .filter('prettyMinutes', function() {
        return function (minutes) {
            if (minutes < 60) return minutes + " Minutes";
            var hours = minutes / 60;
            if (hours == 1) return hours + " Hour";
            return hours + " Hours";
        }
    })

    .filter('timeString', function () {
        return function (date) {
            var d = new Date(date);
            var min = d.getUTCMinutes();
            if (min/10<1) min = '0'+min;
            var ampm = d.getHours() >= 12 ? 'pm' : 'am';
            var hours = d.getHours();
            if (ampm == 'pm') hours = hours-12;
            if (hours == 0) hours = 12;
            return hours+":"+min+" "+ ampm;
        }
    })

    .filter('minuteTime', function() {
        return function(minutes) {
            if (minutes == null) return "";
            if (minutes < 60) {
                return minutes + " minutes";
            }
            var hours = Math.floor(minutes/60);
            var str = hours;
            if (hours == 1) str += " hour ";
            else str += " hours ";
            var min = minutes%60;
            if (min != 0) str += min;
            if (min == 1) str += " minute";
            else str += " minutes";
            return str;
        }
    })

    .filter('mileDistance', function() {
        return function(miles) {
            if (miles == null) return "";
            var str = miles.substring(0,miles.indexOf('.'));
            if (str == 1) str += " mile";
            else str += " miles";
            return str;
        }
    })

    .factory('userService', function() {
        return {
            getUser : function() {
                return JSON.parse(window.localStorage['user'] || null);
            },
            setUser : function(user) {
                window.localStorage['user'] = JSON.stringify(user);
            },
            saveVendorName : function(name) {
                var user = JSON.parse(window.localStorage['user'] || null);
                if (user != null) {
                    user.vendorName = name;
                    window.localStorage['user'] = JSON.stringify(user);
                }
            },
            saveVendorDesc : function(desc) {
                var user = JSON.parse(window.localStorage['user'] || null);
                if (user != null) {
                    user.vendorDesc = desc;
                    window.localStorage['user'] = JSON.stringify(user);
                }
            },
            saveVendorWebsite : function(website) {
                var user = JSON.parse(window.localStorage['user'] || null);
                if (user != null) {
                    user.vendorWebsite = website;
                    window.localStorage['user'] = JSON.stringify(user);
                }
            }
        }
    })

    .factory('locationService', function($q) {
        return {
            getLatLng : function() {
                var deferred = $q.defer();
                if (navigator.geolocation) {
                    navigator.geolocation.getCurrentPosition(function (position) {
                        deferred.resolve({
                            lat: position.coords.latitude,
                            lng: position.coords.longitude
                        });
                    }, function () {
                        deferred.reject();
                    });
                } else {
                    deferred.reject();
                }
                return deferred.promise;
            }
        }
    })

    .config(function ($stateProvider, $urlRouterProvider, $cordovaFacebookProvider) {

        //if (window.cordova.platformId == "browser") {
            var appID = 581544595340073;
            var version = "v2.0"; // or leave blank and default is v2.0
            $cordovaFacebookProvider.browserInit(appID, version);
        //}
        $stateProvider

            .state('app', {
                url: '/app',
                abstract: true,
                templateUrl: 'templates/menu.html',
                controller: 'AppCtrl'
            })

            .state('app.search', {
                url: '/search',
                views: {
                    'menuContent': {
                        templateUrl: 'templates/search.html',
                        controller : 'SearchCtrl'
                    }
                }
            })

            .state('app.markets', {
                url: '/markets',
                views: {
                    'menuContent': {
                        templateUrl: 'templates/markets.html',
                        controller : 'MarketCtrl'
                    }
                }
            })

            .state('app.market', {
                url: '/market/:marketId:marketName',
                views: {
                    'menuContent': {
                        templateUrl: 'templates/market.html',
                        controller: 'ShowMarketCtrl'
                    }
                }
            })

            .state('app.event', {
                url: '/event/:eventId:eventName',
                views: {
                    'menuContent': {
                        templateUrl: 'templates/event.html',
                        controller: 'EventCtrl'
                    }
                }
            })


            .state('app.settings', {
                url: '/settings',
                views: {
                    'menuContent': {
                        templateUrl: 'templates/settings.html',
                        controller : 'SettingsCtrl'
                    }
                }
            });
        // if none of the above states are matched, use this as the fallback
        $urlRouterProvider.otherwise('/app/search');
    });
