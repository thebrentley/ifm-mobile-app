angular.module('starter.controllers', [])

    .controller('AppCtrl', function ($scope, config, $ionicModal, $timeout, userService, $http, $cordovaFacebook, $state, $ionicHistory) {

        // With the new view caching in Ionic, Controllers are only called
        // when they are recreated or on app start, instead of every page change.
        // To listen for when this page is active (for example, to refresh data),
        // listen for the $ionicView.enter event:
        //$scope.$on('$ionicView.enter', function(e) {
        //});

        // Form data for the login modal
        $scope.loginData = {};

        $scope.user = userService.getUser();

        // Create the login modal that we will use later
        $ionicModal.fromTemplateUrl('templates/login.html', {
            scope: $scope
        }).then(function (modal) {
            $scope.modal = modal;
        });

        $ionicModal.fromTemplateUrl('templates/add.html', {
            scope: $scope
        }).then(function (modal) {
            $scope.addModal = modal;
        });

        // Triggered in the login modal to close it
        $scope.closeLogin = function () {
            $scope.modal.hide();
        };

        $scope.closeAdd = function () {
            $scope.addModal.hide();
        };

        // Open the login modal
        $scope.login = function () {
            $scope.modal.show();
        };

        $scope.addEvent = function() {
            $scope.creating = true;
            var obj = $scope.addObject;
            $scope.errors = {
                active : false
            };
            if (obj.marketId == null) {
                $scope.errors.active = true;
                $scope.errors.noMarket = true;
            } else if (obj.marketId != 'add') {
                if (obj.eventName == null || obj.eventName == '') {
                    $scope.errors.active = true;
                    $scope.errors.eventNameEmpty = true;
                }
            } else {
                if (obj.name == null || obj.name == '') {
                    $scope.errors.active = true;
                    $scope.errors.marketEmpty = true;
                }
                if (obj.street == null || obj.street == '') {
                    $scope.errors.active = true;
                    $scope.errors.streetEmpty = true;
                }
                if (obj.city == null || obj.city == '') {
                    $scope.errors.active = true;
                    $scope.errors.cityEmpty = true;
                }
                if (obj.zip == null || obj.zip == '') {
                    $scope.errors.active = true;
                    $scope.errors.zipEmpty = true;
                }
            }

            if ($scope.errors.active) {
                $scope.creating = false;
            } else {
                if ($scope.addObject.marketId === 'add') obj.marketId = null;
                $http({
                    url: config.endpoint + '/add-event',
                    method: 'POST',
                    data: obj
                }).success(function (data) {
                    if (data != null) {
                        $scope.closeAdd();
                        $scope.creating = false;
                    }
                });
            }
        };

        $scope.changeStartTime = function() {
            if ($scope.addObject.endTime.getTime() < $scope.addObject.startTime.getTime()) {
                $scope.addObject.endTime = $scope.addObject.startTime;
            }
        };

        $scope.changeEndTime = function() {
            if ($scope.addObject.endTime.getTime() < $scope.addObject.startTime.getTime()) {
                $scope.addObject.startTime = $scope.addObject.endTime;
            }
        };

        // Open the login modal
        $scope.add = function () {
            $http({
                url : config.endpoint + '/markets',
                method : 'GET'
            }).success(function(data) {
                $scope.markets = data;
            });
            var date = new Date();
            date.setSeconds(0);
            date.setMinutes(0);
            $scope.addObject = {
                startTime : date,
                endTime : date,
                state : 'Idaho'
            };
            $scope.addModal.show();
        };

        $scope.logout = function() {
            userService.setUser(null);
            $scope.user = userService.getUser();
            $ionicHistory.nextViewOptions({
                disableBack: true
            });
            $state.go('app.search');
        };

        // Perform the login action when the user submits the login form
        $scope.doLogin = function () {
            $cordovaFacebook.login(["public_profile", "email"])
                .then(function(response) {
                    if (response.status === 'connected') {
                        var obj = {
                            accessToken : response.authResponse.userID,
                            accessType : 'F'
                        };
                        $http({
                            method : 'POST',
                            url : config.endpoint + '/login',
                            data : obj
                        }).success(function(data) {
                            $http.get("https://graph.facebook.com/v2.2/me", {params: {access_token: response.authResponse.accessToken, fields: "name,gender,location,picture", format: "json" }}).then(function(result) {
                                data.avatarUrl = result.data.picture.data.url;
                                userService.setUser(data);
                                $scope.user = userService.getUser();
                                $scope.closeLogin();
                            });
                        });
                    }
                }, function (error) {
                    // error
                });
        };
    })

    .controller('SearchCtrl', function ($scope, $ionicPlatform, config, $ionicPopover, $http, locationService, $state) {
        $scope.miles = [10,25,50,100,250,500];
        $scope.minutes = [10,20,30,60,120,180];
        $scope.loading = true;

        // .fromTemplateUrl() method
        $ionicPopover.fromTemplateUrl('templates/popover.html', {
            scope: $scope
        }).then(function(popover) {
            $scope.popover = popover;
        });

        $scope.openPopover = function($event) {
            $scope.popover.show($event);
        };
        $scope.closePopover = function() {
            $scope.popover.hide();
        };
        //Cleanup the popover when we're done with it!
        $scope.$on('$destroy', function() {
            $scope.popover.remove();
        });
        // Execute action on hide popover
        $scope.$on('popover.hidden', function() {
            // Execute action
        });
        // Execute action on remove popover
        $scope.$on('popover.removed', function() {
            // Execute action
        });

        var getDateString = function(date) {
            var d = new Date(date);
            var day = d.getDate();
            if (day == 1) day += "st";
            else if (day == 2) day += "nd";
            else if (day == 3) day += "rd";
            else day += "th";
            return months[d.getMonth()]+" "+ day;
        };

        $scope.events = {
            selectParam : function(index, type) {
                if (type == 'miles') {
                    if ($scope.miles[index] == $scope.selectedMile) {
                        $scope.selectedMile = null;
                    } else {
                        $scope.selectedMile = $scope.miles[index];
                    }
                    $scope.selectedMinute = null;
                } else if (type == 'minutes') {
                    if ($scope.minutes[index] == $scope.selectedMinute) {
                        $scope.selectedMinute = null;
                    } else {
                        $scope.selectedMinute = $scope.minutes[index];
                    }
                    $scope.selectedMile = null;
                }
                $scope.groups = [];
                $scope.loading = true;
                $scope.events.search();
            },

            search : function() {
                var obj = {};
                if ($scope.selectedMinute != null) {
                    obj.minutes = $scope.selectedMinute;
                } else if ($scope.selectedMile != null) {
                    obj.miles = $scope.selectedMile;
                }
                locationService.getLatLng().then(function(data) {
                    obj.lat = data.lat+"";
                    obj.lng = data.lng+"";
                    $http({
                        method : 'POST',
                        url : config.endpoint + '/events',
                        data : obj
                    }).success(function(data) {
                        $scope.groups = [];
                        for (elt in data) {
                            var event = data[elt];
                            var startTime = new Date(event.startTime);
                            var identifier = months[startTime.getMonth()] + " " + startTime.getFullYear();
                            var monthIndex = -1;
                            for (e in $scope.groups) {
                                if ($scope.groups[e].identifier == identifier) {
                                    monthIndex = e;
                                    break;
                                }
                            }
                            if (monthIndex == -1) {
                                for (var i=0;i<$scope.groups.length;i++) {
                                    if ($scope.groups[i].month > startTime.getMonth()) {
                                        break;
                                    }
                                }
                                $scope.groups.splice(i, 0, {
                                    divider : true,
                                    identifier : identifier,
                                    month : startTime.getMonth(),
                                    events : []
                                });
                                monthIndex = i;
                            }
                            if ($scope.user != null) {
                                for (u in data[elt].users) {
                                    if (data[elt].users[u].accessToken == $scope.user.accessToken &&
                                        data[elt].users[u].accessType == $scope.user.accessType) {
                                        data[elt].subscribed = true;
                                    }
                                }
                            }
                            for (i=0;i<$scope.groups[monthIndex].events.length;i++) {
                                if ($scope.groups[monthIndex].events[i].startTime > data[elt].startTime) {
                                    break;
                                }
                            }
                            $scope.groups[monthIndex].events.splice(i, 0, data[elt]);
                        }
                        $scope.loading = false;
                        $scope.$broadcast('scroll.refreshComplete');
                    });
                });
            },

            toggleSubscribe : function(event) {
                event.subscribed = !event.subscribed;
                $http({
                    url : config.endpoint + '/subscribe',
                    method : 'POST',
                    data : {
                        accessToken : $scope.user.accessToken,
                        accessType : $scope.user.accessType,
                        subscribed : event.subscribed,
                        eventId : event.id
                    }
                });
            },

            selectEvent : function(event) {
                var params = {
                    eventId :  event.id,
                    eventName : event.name
                };
                $state.go('app.event', params);
            }
        };

        $ionicPlatform.ready(function () {
            $scope.events.search();
        });
    })

    .controller('SettingsCtrl', function ($scope, config, $ionicPopover, $http, userService) {

        if ($scope.user.userType == 'VENDOR') $scope.user.vendor = true;
        else $scope.user.vendor = false;

        $scope.events = {
            changeUserType : function() {
                if ($scope.user.vendor) {
                    $scope.user.userType = 'VENDOR';
                } else {
                    $scope.user.userType = 'USER';
                }
                $http({
                    url : config.endpoint + '/update-user',
                    method : 'POST',
                    data : $scope.user
                }).success(function(data) {
                    if (data === 'SUCCESS') {
                        userService.setUser($scope.user);
                    }
                })
            },

            updateVendorName : function() {
                $scope.vendorNameExists = false;
                $http({
                    url : config.endpoint + '/update-vendor-name',
                    method : 'POST',
                    data : {
                        accessToken : $scope.user.accessToken,
                        accessType : $scope.user.accessType,
                        vendorName : $scope.user.vendorName
                    }
                }).success(function(data) {
                    if (data === 'SUCCESS') {
                        userService.saveVendorName($scope.user.vendorName);
                    } else if (data === 'VENDOR_EXISTS') {
                        $scope.vendorNameExists = true;
                    }
                });
            },

            updateVendorDesc : function() {
                $http({
                    url : config.endpoint + '/update-vendor-desc',
                    method : 'POST',
                    data : {
                        accessToken : $scope.user.accessToken,
                        accessType : $scope.user.accessType,
                        vendorDesc : $scope.user.vendorDescription
                    }
                }).success(function(data) {
                    if (data === 'SUCCESS') {
                        userService.saveVendorDesc($scope.user.vendorDesc);
                    }
                });
            },

            updateVendorWebsite : function() {
                $http({
                    url : config.endpoint + '/update-vendor-website',
                    method : 'POST',
                    data : {
                        accessToken : $scope.user.accessToken,
                        accessType : $scope.user.accessType,
                        vendorWebsite : $scope.user.vendorWebsite,
                    }
                }).success(function(data) {
                    if (data === 'SUCCESS') {
                        userService.saveVendorWebsite($scope.user.vendorWebsite);
                    }
                });
            }
        }
    })

    .controller('MarketCtrl', function ($scope, config, $ionicPopover, $http, $state) {
        var private = {
            getMarkets : function() {
                $http({
                    url : config.endpoint + '/markets',
                    type : 'GET'
                }).success(function(data) {
                    $scope.markets = data;
                });
            }
        };

        $scope.events = {
            refresh : function() {
                private.getMarkets();
                $scope.$broadcast('scroll.refreshComplete');
            },

            openMarket : function(market) {
                var params = {
                    marketId :  market.id,
                    marketName : market.name
                };
                $state.go('app.market', params);
            }
        };

        private.getMarkets();
    })

    .controller('ShowMarketCtrl', function ($scope, config, $ionicPopover, $http, $state, $stateParams, $ionicHistory, uiGmapIsReady) {
        $scope.marketId = $stateParams.marketId;
        $scope.marketName = $stateParams.marketName;

        $scope.map = {
            center: { latitude: 45, longitude: -73 },
            zoom: 15,
            marker : {
                id: 0
            },
            options : {
                draggable : true,
                mapTypeControl : false,
                streetViewControl : false,
                zoomControl : false
            }
        };

        var private = {
            getMarket : function() {
                $http({
                    url : config.endpoint + '/market/'+$scope.marketId,
                    type : 'GET'
                }).success(function(data) {
                    $scope.market = data;
                    for (elt in data.users) {
                        if (data.users[elt].accessToken == $scope.user.accessToken && data.users[elt].accessType == $scope.user.accessType) {
                            $scope.subscribed = true;
                            break;
                        }
                    }
                    $http({
                        url : config.endpoint + '/market/' + $scope.marketId + '/events',
                        type : 'GET'
                    }).success(function(data) {
                        $scope.groups = [];
                        for (elt in data) {
                            var event = data[elt];
                            var startTime = new Date(event.startTime);
                            var identifier = months[startTime.getMonth()] + " " + startTime.getFullYear();
                            var monthIndex = -1;
                            for (e in $scope.groups) {
                                if ($scope.groups[e].identifier == identifier) {
                                    monthIndex = e;
                                    break;
                                }
                            }
                            if (monthIndex == -1) {
                                $scope.groups.push({
                                    divider : true,
                                    identifier : identifier,
                                    events : []
                                });
                                monthIndex = $scope.groups.length-1;
                            }
                            for (u in data[elt].users) {
                                if (data[elt].users[u].accessToken == $scope.user.accessToken &&
                                    data[elt].users[u].accessType == $scope.user.accessType) {
                                    data[elt].subscribed = true;
                                }
                            }
                            $scope.groups[monthIndex].events.push(data[elt]);
                        }
                    });
                    $scope.map.center = {
                        latitude : data.latitude,
                        longitude : data.longitude
                    };
                    $scope.map.marker = {
                        id : 0,
                        coords : {
                            latitude: data.latitude,
                            longitude: data.longitude
                        }
                    };
                    uiGmapIsReady.promise().then(function (maps) {
                        google.maps.event.trigger(maps[0].map, 'resize');
                        jQuery('#event_list').css('height',(jQuery('#body').height()-jQuery('#map').height()-jQuery('.item-divider').height()-20)+'px');
                    });
                });
            }
        };

        $scope.events = {
            refresh : function() {
                private.getMarket();
                $scope.$broadcast('scroll.refreshComplete');
            },

            selectEvent : function(event) {
                $ionicHistory.nextViewOptions({
                    disableBack: true
                });
                var params = {
                    eventId :  event.id,
                    eventName : event.name
                };
                $state.go('app.event', params);
            },

            subscribe : function() {
                $scope.subscribed = !$scope.subscribed;
                $http({
                    url : config.endpoint + '/subscribe-market',
                    method : 'POST',
                    data : {
                        accessToken : $scope.user.accessToken,
                        accessType : $scope.user.accessType,
                        subscribed : $scope.subscribed,
                        marketId : $scope.marketId
                    }
                });
            }
        };

        private.getMarket();
    })

    .controller('EventCtrl', function ($scope, config, locationService, $http, $state, $stateParams, uiGmapIsReady, $cordovaAppAvailability) {
        $scope.eventId = $stateParams.eventId;
        $scope.eventName = $stateParams.eventName;
        $scope.loading = true;

        $scope.map = {
            center: { latitude: 45, longitude: -73 },
            zoom: 15,
            markers : [
                { id : 0 },
                { id : 1 }
            ],
            options : {
                draggable : true,
                mapTypeControl : false,
                streetViewControl : false,
                zoomControl : false
            }
        };

        var private = {
            getEvent : function() {
                locationService.getLatLng().then(function(data) {
                    $scope.map.markers[1].latitude = data.lat;
                    $scope.map.markers[1].longitude = data.lng;
                    $scope.map.markers[1].icon = 'img/bluedot.png';
                    $scope.map.markers[1].loaded = true;
                    $http({
                        url: config.endpoint + '/event/' + $scope.eventId,
                        method: 'GET'
                    }).success(function (data) {
                        $scope.event = data;
                        $scope.vendors = data.users;
                        $scope.map.markers[0].latitude = $scope.event.market.latitude;
                        $scope.map.markers[0].longitude = $scope.event.market.longitude;
                        $scope.map.markers[0].icon = 'img/current-location.png';
                        $http({
                            url: config.endpoint + '/event/location-details',
                            method: 'POST',
                            data: {
                                eventId: $scope.eventId,
                                lat: $scope.map.markers[1].latitude,
                                lng: $scope.map.markers[1].longitude
                            }
                        }).success(function (data) {
                            $scope.locationDetails = data;
                            $scope.loading = false;
                            $scope.$broadcast('scroll.refreshComplete');
                        });
                    });
                });
            }
        };

        $scope.events = {
            openInMaps : function() {
                $cordovaAppAvailability.check('twitter://')
                    .then(function () {
                        // is available
                        console.log("available")
                    }, function () {
                        // not available
                        console.log('not available')
                    });
            },

            openWebsite : function(website) {
                if (website.substring(0,7) != 'http://') {
                    website = 'http://' + website;
                }
                window.open(website, '_blank', 'location=yes');
            },

            refresh : function() {
                private.getEvent();
            }
        };

        private.getEvent();
    });
